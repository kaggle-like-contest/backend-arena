# Credit Concession Scoring Model

## 1. Intro

### 1.1. The challenge

The challenge is a supervised machine learning problem where you must create a credit concession scoring model that replicates
the scoring model of a purple financial institution that predicts the risk of a person not paying his debts in the next year.
The model must have the following properties:

- It must be a regression model;
- It must output an integer score in the range of \[0,1000\] where 0 represents the highest risk of default.

### 1.2. The evaluation

Models will be assessed by the **[Root Mean Square Error (RMSE)](https://en.wikipedia.org/wiki/Root-mean-square_deviation)**
metric, the lower the RMSE, the better the model replicates the scoring. Eventual draws will be resolved by the submission
timestamp where early submitted predictions take precedence.

### 1.3. The submission file

The submission file must follow the rules bellow:

- It must be named according the **PREDICTION_FILE** field from the `./settings.py` file;
- It must be written on the `./output` folder;
- It must pass all validations present on the `./submission/validator.py` file.

Example:

```csv
KEY;SCORE
<key1>;<score1>
<key2>;<score2>
...;...
```

### 1.4. The deadline

The challenge will take place from 3rd May 2020 (23:59h) to 29th May 2020 (23:59h).

### 1.5. The prize

The 1st and 2nd best prediction will earn a R$25.00 Uber Eats voucher.

## 2. Setup

The following steps will get you a copy of the project up and running on your local machine:

1. Download or git clone the project to your machine;
2. **[Request](https://docs.gitlab.com/ee/user/project/members/#project-membership-and-requesting-access)** access to the code to get your **api key**;
3. Create a `./settings.py` file by coping the provided `./settings_example.py` file and update the **API_KEY** field with your **api key**.

> **Note**: You won't be able to submit predictions without an **api key**.

## 3. Modelling

Follow the steps bellow to train and validate your model. An example can be found on `./models/linear_regression/model.py` file.

### 3.1. Train

You must train your model using the data from the `./datasets/train.csv` file. It contains around 60k rows with the **key**,
**score** (target) and  **[features](./docs/features.md)** (predictors) variables to train your regression model with.
For this step, you can drop the **key** column.

### 3.2. Validate

With your model ready to be validated, you must predict the credit score using the data from the `./datasets/validate.csv` file.
It contains around 30k rows with the the **key** and the unseen **[features](./docs/features.md)** (predictors) variables. For
this step, you must keep the **key** column. Also, you must write your prediction into a file according to rules specified on
the  **[readme](./README.md)** file, section 1.3.

## 3. Submitting

Run the `./submit.py` file to submit your prediction.

> **Note**: You won't be able to submit predictions without an **api key**.

## 4. Ranking

On each submission you will get the top 5 best predictions. An online web version can be found **[here]()**.