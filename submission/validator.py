import pandas
import logging


class Validator(object):

    def __init__(self):
        self._log = logging
        self._setup()

    def _setup(self):
        self._log.basicConfig(
            level=logging.INFO,
            datefmt='%d-%m-%y | %H:%M:%S',
            format='%(asctime)s | %(levelname)s | %(module)s.%(funcName)s: %(message)s'
        )

    def validate(self, file: str) -> bool:
        self._log.info(msg='Starting prediction file validation: {file}'.format(file=file))
        try:
            dataframe = pandas.read_csv(file, sep=';')
            rows, cols = dataframe.shape
            if cols != 2:
                raise ValueError('Wrong number of cols. Expected {truth}, got {value}.'.format(
                    truth=2,
                    value=cols
                ))
            if rows != 29958:
                raise ValueError('Wrong number of rows. Expected {truth}, got {value}.'.format(
                    truth=29958,
                    value=rows
                ))
            header = list(dataframe.columns)
            if header != ['KEY', 'SCORE']:
                raise ValueError('Wrong header. Expected {truth}, got {value}.'.format(
                    truth=['KEY', 'SCORE'],
                    value=header
                ))
            key = dataframe['KEY']
            if not pandas.api.types.is_integer_dtype(key.dtype):
                raise ValueError('Wrong {col} col type. Expected {truth}, got {value}.'.format(
                    col='KEY',
                    truth='int like type',
                    value=key.dtype
                ))
            score = dataframe['SCORE']
            if not pandas.api.types.is_integer_dtype(score.dtype):
                raise ValueError('Wrong {col} col type. Expected {truth}, got {value}.'.format(
                    col='SCORE',
                    truth='int like type',
                    value=score.dtypes
                ))
            self._log.info(msg='Prediction file is valid.')
            return True
        except Exception as exc:
            self._log.warning(msg=exc)
            self._log.error(msg='Prediction file is invalid.')
            return False
