import json
import gzip
import logging
import settings
import requests
from submission.validator import Validator

ORDINAL_SUFFIX = {'1': 'st', '2': 'nd', '3': 'rd', '4': 'th', '5': 'th'}


class Submitter(object):

    def __init__(self):
        self._file = 'output/' + settings.PREDICTION_FILE
        self._url = settings.API_URL
        self._key = settings.API_KEY
        self._log = logging
        self._setup()

    def _setup(self):
        self._log.basicConfig(
            level=logging.INFO,
            datefmt='%d-%m-%y | %H:%M:%S',
            format='%(asctime)s | %(levelname)s | %(module)s.%(funcName)s: %(message)s'
        )

    def run(self):
        is_valid = Validator().validate(file=self._file)
        if is_valid:
            prediction = Submitter._get_prediction(file=self._file)
            self._submit_prediction(prediction=prediction, url=self._url, key=self._key)
            self._get_ranking(url=self._url, key=self._key)

    @staticmethod
    def _get_prediction(file: str) -> dict:
        prediction = dict()
        with open(file=file, mode='r', encoding='utf8') as fp:
            fp.readline()
            line = fp.readline()
            while line:
                line = line.rstrip().split(sep=';')
                prediction[line[0]] = line[1]
                line = fp.readline()
        return prediction

    def _submit_prediction(self, prediction: dict, url: str, key: str):
        self._log.info(msg='Starting prediction file submission.')
        try:
            payload = dict(metric='rmse', prediction=prediction)
            payload = gzip.compress(json.dumps(payload).encode('utf-8'))
            response = requests.post(
                url=url,
                data=payload,
                headers={
                    'x-api-key': key,
                    'content-encoding': 'gzip',
                    'content-type': 'application/json'
                }
            )
            if response.status_code is requests.codes.ok:
                self._log.info(msg='Prediction file submission succeeded.')
                body = json.loads(response.text)['body']
                self._log.info(msg='You got a RMSE of {rmse}.'.format(rmse=body['metric']))
            else:
                self._log.warning(msg=json.loads(response.text)['message'])
                response.raise_for_status()
        except Exception:
            self._log.error(msg='Prediction file submission failed.')

    def _get_ranking(self, url: str, key: str):
        self._log.info(msg='Getting top five predictions.')
        try:
            params = dict(metric='rmse', top=5)
            response = requests.get(
                url=url,
                params=params,
                headers={'x-api-key': key}
            )
            if response.status_code is requests.codes.ok:
                self._log.info(msg='Getting top five predictions succeeded.')
                body = json.loads(response.text)['body']
                self._log.info(msg='The top five predictions are:')
                for position, prediction in enumerate(body['ranking']):
                    self._log.info(msg='- {position}{sufix} - {name} - {metric} - {value} - {date}.'.format(
                        position=position+1,
                        sufix=ORDINAL_SUFFIX['{ordinal}'.format(ordinal=position+1)],
                        name=prediction['name'],
                        metric=prediction['metric'],
                        value=prediction['value'],
                        date=prediction['date']
                    ))
            else:
                self._log.warning(msg=json.loads(response.text)['message'])
                response.raise_for_status()
        except Exception:
            self._log.error(msg='Getting top five predictions failed.')
