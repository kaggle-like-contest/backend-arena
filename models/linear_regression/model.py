import pandas
import settings
from sklearn.linear_model import LinearRegression


if __name__ == '__main__':

    # 1. Train a linear regression model

    train = pandas.read_csv('../../datasets/train.csv', sep=';')
    target = train['SCORE'].to_numpy()
    predictors = train.drop(labels=['KEY', 'SCORE'], axis=1).to_numpy()
    model = LinearRegression().fit(X=predictors, y=target)

    # 2. Predict the score on unseen data

    validation = pandas.read_csv('../../datasets/validate.csv', sep=';')
    unseen = validation.drop(labels=['KEY'], axis=1).to_numpy()
    prediction = model.predict(X=unseen)
    prediction = map(lambda score: int(score), prediction)
    prediction = map(lambda score: score if score < 1000 else 1000, prediction)
    prediction = map(lambda score: score if score > 0 else 0, prediction)

    # 3. Save the prediction

    prediction = pandas.concat(objs=[validation['KEY'], pandas.DataFrame(prediction, columns=['SCORE'])], axis=1)
    prediction.to_csv('../../output/' + settings.PREDICTION_FILE, sep=';', header=True, index=False)
