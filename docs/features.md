# Features

### General

+ **KEY**: Unique id representing the Brazilian social security number (CPF);
+ **SCORE**: Credit concession score (target variable).

### Banks

+ **QTBANCOS**: number of active bank accounts;
+ **QTBANCOS_360D**: number of bank accounts active in the last 360 days;
+ **QTBANCOS_720D**: number of bank accounts active in the last 720 days.

### Queries

+ **VQT_CONSSEG_360D**: number of queries made by insurance companies in the last 360 days;
+ **VQT_CONSSEG_720D**: number of queries made by insurance companies in the last 720 days;
+ **VTPPRI_CONSCRDSTS_5A**: time in days since the first query on the credit bureau*;
+ **VNUMAXEMP_CONSSTS_180D**: maximum number of queries that the same company made to the CPF in the last 180 days*;
+ **VNUMAXEMP_CONSSTS_360D**: maximum number of queries that the same company made to the CPF in the last 360 days*;
+ **VNUMAXEMP_CONSSTS_720D**: maximum number of queries that the same company made to the CPF in the last 720 days*.
+ **XQT_CONSFIN_90D**: number of queries made by lender companies in the last 90 days;
+ **XQT_CONSFIN_180D**: number of queries made by lender companies in the last 180 days;
+ **XNUEMP_CONSSTS_5D**: number of queries made by companies in the last 5 days*;
+ **XNUEMP_CONSSTS_10D**: number of queries made by companies in the last 10 days*;
+ **XNUEMP_CONSSTS_15D**: number of queries made by companies in the last 15 days*;
+ **XQTPDREC_CONSSTS_P20DU60D**: weighted amount of query recency over 20-day periods in the last 60 days*;
+ **XQTPDREC_CONSSTS_P20DU120D**: weighted amount of query recency over 20-day periods in the last 120 days*.

### Debts

+ **VQTRESTRESGBCOPRI**: amount of overdue and settled debts, included by the group of large private banks;
+ **VQTRESTRESGBCOOUT**: amount of overdue and settled debts, included by the group of small and medium-sized private banks;
+ **VINDTODASRESTRCOPART**: indicates that all overdue debts (settled or not) are co-participation;
+ **VQTRESTRORIGFINRESA6M**: amount of overdue debts settled over 180 days, included by lender companies;
+ **VQTRESTRORIGTELRESU12M**: amount of overdue debts settled over 365 days, included by telecoms and utilities companies (energy, water, etc.).
+ **XFLRESTRATI90D**: indicates that has overdue debts (settled or not) in the last 90 days;
+ **XPERMAXSEMRESTR**: maximum time in days without overdue debts not settled since the first inclusion of any overdue debt (settled or not);
+ **XQTRESTRRESA6M360DPI**: amount of overdue debts settled over 180 days and between 181 and 360 days of inclusion;
+ **XQTRESTRATIATRASO90D**: amount of overdue debts not settled of more than 90 days;
+ **XQTRESTRORIGBCORESU6M**: amount of overdue and settled debts in the last 180 days, included by multiple banks.

### Notes

+ \* Insurance and telecommunication companies are excluded from the metric.